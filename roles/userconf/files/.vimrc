" have syntax highlighting in terminals which can display colours:
if has('syntax') && (&t_Co > 2)
  syntax on
endif
set hlsearch
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd Filetype perl setlocal omnifunc=syntaxcomplete#Complete
let skip_defaults_vim = 1
set mouse=r
set showmatch " Shows matching brackets
set smarttab " Autotabs for certain code
set shiftwidth=4
