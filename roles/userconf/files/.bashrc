# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
export HISTCONTROL=ignoredups
export HISTSIZE=100000
export HISTFILESIZE=1000000
export HISTTIMEFORMAT='%Y-%b-%d %H:%M '
#########################
EDITOR=vim
export EDITOR
export PATH=~/.vim/bin:~/.local/bin:$PATH
export SSH_AUTH_SOCK
alias editor=$EDITOR
#export PAGER="most"
# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
shopt -s histappend

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(lesspipe)"
if [ $(id -u) -eq 0 ]; then
	PS1='\[\033[1;31m\][\u@\[\033[1;33m\]\h:\[\033[1;31m\]\w]#\[\033[0m\] '
else
	PS1='\[\033[1;34m\][\u@\[\033[1;36m\]\h:\[\033[1;34m\]\w]$\[\033[0m\] '
fi
# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*|tmux*|screen*)
    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD/$HOME/~}\007"'
    PROMPT_COMMAND="$PROMPT_COMMAND;history -a;history -r;history 1 >> ${HOME}/.bash_history_endless"
    ;;
*)
    ;;
esac

if [ "$TERM" != "dumb" ]; then
    eval "`dircolors -b`"
    alias ls='ls --color=auto'
fi

if [ -d ~/bin ] ; then
    PATH=~/bin:"${PATH}"
fi

LS_COLORS='rs=0:di=01;36:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:';
export LS_COLORS

# some more ls aliases
alias ll='ls -lah --color=auto'
alias iconvcu='iconv -c -f cp1251 -t utf8'
alias grep='grep --color=auto'
alias acs='apt-cache search'
alias agi='apt-get install'
alias ccal='ccal -m'
alias signal='signal-desktop --no-sandbox'
alias gitlc='git log --name-status HEAD^..HEAD'
alias trans='translate -nw -l de-en'
alias gitbrufix='git fetch origin master ; git reset --hard FETCH_HEAD ; git clean -df ; git status'
alias gitbrufx='git fetch --all ; git reset --hard origin/master ; git pull origin master ; git status'
alias wget='wget --no-check-certificate '
#alias mplayer='mplayer --no-audio-display'
# turn off caps lock (crapslock)
alias capsoff='xdotool key Caps_Lock'
alias tcaly='task calendar y '
alias diff='diff --color=always --suppress-common-lines -y '
#set volume +/-5% up/down without shitty gui
alias voup='pactl set-sink-volume @DEFAULT_SINK@ +5% '
alias vodo='pactl set-sink-volume @DEFAULT_SINK@ -5% '
alias systemctl='systemctl --no-pager -l '
eval "$(thefuck --alias fuck)"

if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

# Sexy git stuff
GIT_PROMPT_ONLY_IN_REPO=1
GIT_PROMPT_THEME=Single_line

if [ -d ~/bin/.bash-git-prompt ]; then
    source ~/bin/.bash-git-prompt/gitprompt.sh
fi

. /usr/share/autojump/autojump.sh

#if [ -z "$SSH_AUTH_SOCK" ] ; then
#	eval `ssh-agent -s`
#	export SSH_AUTH_SOCK
#	ssh-add ~/.ssh/id_rsa
#fi

# Start the gpg-agent if not already running
if ! pgrep -x -u "${USER}" gpg-agent >/dev/null 2>&1; then
  gpg-agent --daemon --use-standard-socket --write-env-file "${HOME}/.gpg-agent-info" >/dev/null 2>&1
fi
export GPG_AGENT_INFO
# functions for the lazy one
f() { find . -iname "*${1}*"; }

