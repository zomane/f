#!/bin/bash

# shell scipt to prepend i3status with more stuff

i3status --config ~/.i3status.conf | while :
do
        read line
        LG=$(setxkbmap -query | awk '/layout/{print $2}'| tr '[a-z]' '[A-Z]')
	#WET=$(ansiweather -l vienna,at -u metric -s false -a false  | awk '{ print $6,$7,$11*3.6, "km/h" ,$13 }')
	WET=$(ansiweather -l vienna,at -u metric -s false -a false  | awk '{ print $5,$6,$10*3.6, "km/h" ,$12 }')
        MEM=$(vmstat -s | awk  ' $0 ~ /total memory/ {total=$1 } $0 ~ /total swap/ {swap=$1 } $0 ~/free memory/ {free=$1}  $0 ~/free swap/ {freeswap=$1}  $0 ~/buffer memory/ {buffer=$1} $0 ~/cache/ {cache=$1} END{printf "used_mem: %3.0f%", (total+swap-free-freeswap-buffer-cache)/(total+swap)*100}')
        echo " $LG | $WET | $MEM | $line" || exit 1
done

